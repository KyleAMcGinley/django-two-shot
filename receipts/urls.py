from django.urls import path
from .views import show_receipts, create_receipt, expense_category_list, account_list, create_expense_category, create_account

urlpatterns = [
    path("", show_receipts, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", expense_category_list, name="category_list"),
    path("categories/create/", create_expense_category, name="create_category"),
    path("accounts/", account_list, name="account_list"),
    path("accounts/create/", create_account, name="create_account")
]
