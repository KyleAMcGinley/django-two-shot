from django.shortcuts import render, redirect
from .models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from .forms import CreateReceiptForm, CreateExpenseCategoryForm, CreateAccountForm


# Create your views here.

@login_required(redirect_field_name='login')
def show_receipts(request):
    receipt_list = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_list":receipt_list
    }
    return render(request, "receipts/list.html", context)


@login_required(redirect_field_name='login')
def create_receipt(request):
    if request.method == 'POST':
        form = CreateReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect('home')
    else:
        form = CreateReceiptForm()

    context = {
        'form':form
    }

    return render(request, 'receipts/create.html', context)

def expense_category_list(request):
    categories = ExpenseCategory.objects.filter(owner = request.user)
    context = {
        "category_list":categories,
    }
    return render(request, 'receipts/categories.html', context)

def account_list(request):
    account_list = Account.objects.filter(owner = request.user)
    context = {
        "account_list":account_list
    }
    return render(request, 'receipts/accounts.html', context)

@login_required(redirect_field_name='login')
def create_expense_category(request):
    if request.method == 'POST':
        form = CreateExpenseCategoryForm(request.POST)
        if form.is_valid():
            expense_category = form.save(False)
            expense_category.owner = request.user
            expense_category.save()
            return redirect("category_list")
    else:
        form = CreateExpenseCategoryForm()

    context = {
        "form":form
    }

    return render(request, 'receipts/create_category.html', context)

@login_required(redirect_field_name='login')
def create_account(request):
    if request.method == 'POST':
        form = CreateAccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect('account_list')
    else:
        form = CreateAccountForm()

    context = {
        "form": form
    }

    return render(request, 'receipts/create_account.html', context)
